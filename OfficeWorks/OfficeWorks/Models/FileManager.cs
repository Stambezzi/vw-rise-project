﻿using System.IO;
using System;
using Newtonsoft.Json;
using Microsoft.Win32;
using System.Windows.Controls;
using OfficeWorks.Models.CustomObjects;
using System.Collections.Generic;
using System.Linq;
using OfficeWorks.ModelViews;
using OfficeWorks.ModelViews.Tools;
using OfficeWorks.View.UserControls;
using System.Windows;

namespace OfficeWorks.Models
{
    public class FileManager
    {
        public static HashSet<CustomObjectBase> businessObjects = new HashSet<CustomObjectBase>();
        public static string CurrentFile = "";

        private string GetProjectPath()
        {
            string currentDirectory = AppDomain.CurrentDomain.BaseDirectory;
            return Directory.GetParent(currentDirectory).Parent.FullName;
        }

        private Canvas GetCurrentCanvas()
        {
            return MainWindow.Instance.MainCanvas;
        }

        //Serialize
        private void Serialize(string filePath)
        {
            string serializedObject = JsonConvert.SerializeObject(businessObjects, Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            });

            using (StreamWriter streamWriter = new StreamWriter(filePath))
            {
                streamWriter.Write(serializedObject);
            }
        }
        //Deserialize
        public List<CustomObjectBase> Deserialize(string filePath)
        {
            ClearCanvas();
            CurrentFile = filePath;
            return ReadFromFileAddToCanvas(filePath);
        }

        private List<CustomObjectBase> ReadFromFileAddToCanvas(string filePath)
        {
            string json = File.ReadAllText(filePath);

            var objectList = JsonConvert.DeserializeObject<List<CustomObjectBase>>(json, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            });

            foreach (CustomObjectBase item in objectList)
            {
                var obj = item.Create(GetCurrentCanvas());
            }

            return objectList;
        }
        private List<CustomObjectBase> ReadFromFile(string filePath)
        {
            string json = File.ReadAllText(filePath);

            var objectList = JsonConvert.DeserializeObject<List<CustomObjectBase>>(json, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            });

            //foreach (CustomObjectBase item in objectList)
            //{
            //    var obj = item.Create(GetCurrentCanvas());
            //    GetCurrentCanvas().Children.Remove(obj.UI);
            //}

            return objectList;
        }

        public void SaveFileAs()
        {
            var saveDialog = new SaveFileDialog();
            saveDialog.Filter = "OfficeWorks | *.ow";
            saveDialog.InitialDirectory = GetProjectPath();
            bool? isSuccessfulSave = saveDialog.ShowDialog();
            if (isSuccessfulSave == true)
            {
                //SERIALIZE
                var filePath = saveDialog.FileName;
                Serialize(filePath);
            }
        }

        public void SaveFile()
        {
            if (CurrentFile == null || CurrentFile == "")
            {
                var saveDialog = new SaveFileDialog();
                saveDialog.Filter = "OfficeWorks | *.ow";
                saveDialog.InitialDirectory = GetProjectPath();
                bool? isSuccessfulSave = saveDialog.ShowDialog();
                if (isSuccessfulSave == true)
                {
                    var filePath = saveDialog.FileName;
                    Serialize(filePath);
                    CurrentFile = filePath;
                }
            }
            else
            {
                Serialize(CurrentFile);
            }
            return;
        }

        public string OpenFile()
        {
            var openDialog = new OpenFileDialog();

            openDialog.Filter = "Select an OfficeWorks file | *.ow";
            openDialog.Multiselect = false;
            openDialog.InitialDirectory = GetProjectPath();
            openDialog.Title = "Select a room";
            string filePath = "";

            bool? isSuccesfulOpen = openDialog.ShowDialog();
            if (isSuccesfulOpen == true)
            {
                //DESERIALIZE(Whole path)
                filePath = openDialog.FileName;
                Deserialize(filePath);
            }
            return filePath;
        }
        public string ImportFile()
        {
            var openDialog = new OpenFileDialog();

            openDialog.Filter = "Select an OfficeWorks file | *.ow";
            openDialog.Multiselect = false;
            openDialog.InitialDirectory = GetProjectPath();
            openDialog.Title = "Select a room";
            string filePath = "";

            bool? isSuccesfulOpen = openDialog.ShowDialog();
            if (isSuccesfulOpen == true)
            {
                //DESERIALIZE(Whole path)
                filePath = openDialog.FileName;
                Import(filePath);
            }
            return filePath;
        }
        private void Import(string filePath)
        {
            List<CustomObjectBase> buildingObj = ReadFromFile(filePath);
            CustomComplexObj importedObj = new CustomComplexObj(0, 0, buildingObj, 0, "Imported Object");
            importedObj.Create(GetCurrentCanvas());
        }

        public void ClearCanvas()
        {
            CurrentFile = "";
            ZoomBar.ZoomTextBox.Text = "100";
            PanTool.PanOffset = new Vector();
            SelectionTool.SelectedObjs.Clear();
            businessObjects.Clear();

            var canvasChildren = GetCurrentCanvas().Children;
            canvasChildren.Clear();
            MainWindow.Instance.InitializeBackground();
        }

        internal static CustomObjectBase GetCustomObjectFromID(Guid id)
        {
            if (!businessObjects.Any(x => x.UIID == id))
            {
                return null;
            }
            var obj = businessObjects.First(x => x.UIID == id);
            if (obj != null)
            {
                if (obj.RootID == Guid.Empty)
                {
                    return obj;
                }
                else
                {
                    return GetCustomObjectFromID(obj.RootID);
                }
            }
            return null;
        }
    }
}


