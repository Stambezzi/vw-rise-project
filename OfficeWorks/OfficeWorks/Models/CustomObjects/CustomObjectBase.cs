﻿using Newtonsoft.Json.Linq;
using OfficeWorks.ModelViews;
using OfficeWorks.ModelViews.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace OfficeWorks.Models.CustomObjects
{
    // #1 - this one's purely cosmetic but I think it'd be neat to name these OWObjects
    // #2 - Agreed
    public abstract class CustomObjectBase : IBusinessObserver
    {
        
        private double _positionX;
        public double PositionX
        {
            get { return _positionX; }
            set
            {
                _positionX = value;
                Recreate();
            }
        }

        private double _positionY;
        public double PositionY
        {
            get { return _positionY; }
            set
            {
                _positionY = value;
                Recreate();
            }
        }

        private double _rotation;
        public double Rotation
        {
            get { return _rotation; }
            set
            {
                _rotation = value % 360;
                Recreate();
            }
        }

        private double _scaleX = 1;
        public double ScaleX
        {
            get { return _scaleX; }
            set
            {
                _scaleX = value;
                Recreate();
            }
        }

        private double _scaleY = 1;
        public double ScaleY
        {
            get { return _scaleY; }
            set
            {
                _scaleY = value;
                Recreate();
            }
        }

        public string Name { get; set; }
        public Guid UIID { get; set; }

        public Guid RootID { get; set; }

        //We could add the color and line thickness as props too
        public CustomObjectBase(double positionX, double positionY, double rotation, string name)
        {
            this.PositionX = positionX;
            this.PositionY = positionY;

            this.Rotation = rotation;

            this.Name = name;
            this.ScaleX = 1;
            this.ScaleY = 1;
        }

        protected void InitializeScreenPosition(FrameworkElement element)
        {
            var newPos = new Point(PositionX, PositionY);
            newPos = Vector2DMath.WorldToScreenPoint(newPos);
            InitializePosition(element, newPos, Rotation, new Point(ScaleX, ScaleY));
        }
        protected void InitializePosition(FrameworkElement element, Point newPos, double rotation, Point scale)
        {
            Transforms.TranslateTo(element, newPos.X, newPos.Y);
            Transforms.RotateTo(element, rotation);
            Transforms.ScaleTo(element, scale.X, scale.Y);
        }

        public static bool RenderShape(IUIContainer shape)
        {
            try
            {
                //Models.FileManager.businessObjects.Add(shape);
                MainWindow.Instance.MainCanvas.Children.Add(shape.UI);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private IUIContainer InitializeElement(IUIContainer container, Canvas canvas)
        {
            this.UIID = container.ID;
            canvas.Children.Add(container.UI);
            if (container.Root == null)
            {
                InitializeScreenPosition(container.UI);
            }
            else
            {
                InitializePosition(container.UI, new Point(PositionX, PositionY), Rotation, new Point(ScaleX, ScaleY));
            }

            return container;
        }

        public IUIContainer Create(Canvas canvas)
        {
            var element = CreateUI();

            element = InitializeElement(element, canvas);
            FileManager.businessObjects.Add(this);
            if(this.RootID == Guid.Empty)
            {

				SetUIElementRoot(this);
			}

			return element;
        }

        private void SetUIElementRoot(CustomObjectBase obj)
        {
            if(obj is CustomComplexObj comp)
            {
                foreach(CustomObjectBase container in comp.BuildingObjects)
                {
                    SetUIElementRoot(container);
                }
            }

            if (obj.RootID!=Guid.Empty)
            {
                CustomObjectBase rootObj = FileManager.GetCustomObjectFromID(obj.RootID);
                if(rootObj is CustomComplexObj compl)
                {
                    obj.GetUIElement().Root = compl;
                }
            }
        }

        public virtual IUIContainer CreateTemp(Canvas canvas)
        {
            var element = DrawTemp();
            var obj = InitializeElement(element, canvas);
            obj.UI.IsHitTestVisible = false;
            return obj;
        }

        public abstract IUIContainer CreateUI();

        protected IUIContainer Recreate()
        {
            if (GetUIElement() == null)
            {
                return null;
            }
            Canvas canvas = MainWindow.Instance.MainCanvas;
            CustomComplexObj? root = GetUIElement().Root;
            if (root != null)
            {
                return root.Recreate();
            }
            bool isSelected = false;
            if (SelectionTool.SelectedObjs.Contains(GetUIElement()))
            {
                isSelected = true;
            }
            DeletionTool.Delete(GetUIElement());
            canvas.InvalidateVisual();
            this.UIID = Guid.Empty;
            var container = this.Create(canvas);
            //FileManager.businessObjects.Add(this);
            if (isSelected == true)
            {
                SelectionTool.Select(GetUIElement());
            }
            container.Root = root;
            return container;
        }
        //virtual get info method for getting common info, overriden for other objects
        public virtual IUIContainer DrawTemp()
        {
            var element = CreateUI();
            element.DrawTemp();
            return element;
        }

        //virtual get info method for getting common info, overriden for other objects

        public void TranslateBy(double offsetX, double offsetY)
        {
            PositionX += offsetX;
            PositionY += offsetY;
            //Transforms.Translate(this.visualElement, offsetX, offsetY);
        }

        public void TranslateTo(double newPositionX, double newPositionY)
        {
            double offsetX = newPositionX - PositionX;
            double offsetY = newPositionY - PositionY;

            TranslateBy(offsetX, offsetY);
        }

        public void RotateBy(double rotation)
        {
            Rotation += rotation;
            //Transforms.Rotate(this.visualElement, rotation);
        }

        public void RotateTo(double newRotation)
        {
            double rotationAmount = newRotation - Rotation;

            RotateBy(rotationAmount);
        }

        public void ScaleBy(double scaleAmountX, double scaleAmountY)
        {
            ScaleX *= scaleAmountX;
            ScaleY *= scaleAmountY;

            //Transforms.Scale(this.visualElement, scaleAmountX, scaleAmountY);
        }

        public void ScaleTo(double newScaleX, double newScaleY)
        {
            double scaleDiffX = newScaleX / ScaleX;
            double scaleDiffY = newScaleY / ScaleY;

            ScaleBy(scaleDiffX, scaleDiffY);
        }


        protected Panel GetPropertyUI(string propertyName, double value, Action<double> updateValue, int tabLevel)
        {
            var infoPanel = new DockPanel();
            infoPanel.Children.Add(new TextBlock() { Text = $"{propertyName}:", FontSize = 14, HorizontalAlignment = HorizontalAlignment.Left });
            TextBox textBox = new TextBox();
            textBox.Text = Math.Round(value, 2).ToString();
            textBox.LostFocus += (sender, e) =>
            {
                if (double.TryParse(textBox.Text, out var newValue))
                {
                    updateValue(newValue);
                }
            };

            infoPanel.Children.Add(textBox);
            infoPanel.Margin = new Thickness(tabLevel * 5, 0, 0, 0);
            return infoPanel;
        }
        public virtual void FillInfoPanel(Panel infoPanel, int marginLevel = 0)
        {
            ClearInfoPanel(infoPanel);
            AddToInfoPanel(infoPanel, marginLevel);
        }
        public virtual void ClearInfoPanel(Panel infoPanel)
        {
            infoPanel.Children.Clear();
        }
        public virtual void AddToInfoPanel(Panel infoPanel, int marginLevel)
        {
            Thickness standardMargin = new Thickness(5 * marginLevel, 0, 0, 10);

            TextBox NameField = new TextBox();
            NameField.Text = this.Name;
            NameField.LostFocus += (sender, e) => this.Name = NameField.Text;
            NameField.Margin = standardMargin;
            infoPanel.Children.Add(NameField);

            infoPanel.Children.Add(
                GetPropertyUI(
                    "Position X",
                    PositionX,
                    newPositionX => PositionX = newPositionX,
                    marginLevel)
                );
            var posYField = (
                GetPropertyUI(
                    "Position Y",
                    PositionY,
                    newPositionY => PositionY = newPositionY,
                    marginLevel));
            posYField.Margin = standardMargin;
            infoPanel.Children.Add(posYField);

            var rotationField = (
                GetPropertyUI(
                    "Rotation",
                    Rotation,
                    newRotation => Rotation = newRotation,
                    marginLevel));
            rotationField.Margin = standardMargin;
            infoPanel.Children.Add(rotationField);

            infoPanel.Children.Add(
                GetPropertyUI(
                    "Scale X",
                    ScaleX,
                    newScaleX => ScaleX = newScaleX,
                    marginLevel));

            var scaleYField = (
                GetPropertyUI(
                    "Scale Y",
                    ScaleY,
                    newScaleY => ScaleY = newScaleY,
                    marginLevel));
            scaleYField.Margin = standardMargin;
            infoPanel.Children.Add(scaleYField);
        }

        public IUIContainer? GetUIElement()
        {
            return MainWindow.Instance.GetContainer(this.UIID);
        }

        protected Point GetScreenPosition()
        {
            var totalPosition = Vector2DMath.WorldToScreenPoint(new Point(PositionX, PositionY));

            return totalPosition;
        }

        protected static LineSegment CreateLineSegmentWithDistance(double distance)
        {
            // Calculate the endpoint coordinates
            double x = distance;
            double y = 0;
            Point endPoint = new Point(x, y);

            // Create a LineSegment with the starting and endpoint
            LineSegment lineSegment = new LineSegment(endPoint, isStroked: true);

            return lineSegment;
        }
    }
}
