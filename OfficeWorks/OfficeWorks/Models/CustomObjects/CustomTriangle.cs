﻿using OfficeWorks.ModelViews.Tools;
using OfficeWorks.ModelViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace OfficeWorks.Models.CustomObjects
{
    internal class CustomTriangle : CustomObjectBase
    {
        private double _point2X;
        public double Point2X
        {
            get { return _point2X; }
            set
            {
                this._point2X = value;
                if (GetUIElement() != null)
                {
                    Recreate();
                }
            }
        }
        private double _point2Y;
        public double Point2Y
        {
            get { return _point2Y; }
            set
            {
                this._point2Y = value;
                if (GetUIElement() != null)
                {
                    Recreate();
                }
            }
        }

        private double _point3X;
        public double Point3X
        {
            get { return this._point3X; }
            set
            {
                this._point3X = value;
                if (GetUIElement() != null)
                {
                    Recreate();
                }
            }
        }
        private double _point3Y;
        public double Point3Y
        {
            get { return this._point3Y; }
            set
            {
                this._point3Y = value;
                if (GetUIElement() != null)
                {
                    Recreate();
                }
            }
        }
        public CustomTriangle(double point1X, double point1Y, double point2X, double point2Y, double point3X, double point3Y, double rotation, string name = "Triangle") : base(point1X, point1Y, rotation, name)
        {
            this.Point2X = point2X;
            this.Point2Y = point2Y;

            this.Point3X = point3X;
            this.Point3Y = point3Y;
        }
        public override IUIContainer CreateUI()
        {
            LineSegment line1 = new LineSegment(new Point((Point2X - PositionX) * ZoomHandler.ZoomLevelScale,
                (Point2Y - PositionY) * ZoomHandler.ZoomLevelScale), isStroked: true);
            LineSegment line2 = new LineSegment(new Point((Point3X - PositionX) * ZoomHandler.ZoomLevelScale,
                (Point3Y - PositionY) * ZoomHandler.ZoomLevelScale), isStroked: true);

            PathFigure pathFigure = new PathFigure();
            pathFigure.StartPoint = new Point(0, 0);
            pathFigure.Segments.Add(line1);
            pathFigure.Segments.Add(line2);
            pathFigure.IsClosed = true;

            PathGeometry pathGeometry = new PathGeometry();
            pathGeometry.Figures.Add(pathFigure);

            Path triangle = new Path();
            triangle.Stroke = Brushes.Black;
            triangle.StrokeThickness = ScaleHandler.CalculateLineThickness();
            triangle.Data = pathGeometry;

            return new UIPathContainer(triangle);
        }

        public override void AddToInfoPanel(Panel infoPanel, int marginLevel)
        {
            base.AddToInfoPanel(infoPanel, marginLevel);
            Thickness standardMargin = new Thickness(5 * marginLevel, 0, 0, 10);

            infoPanel.Children.Add(GetPropertyUI(
                "Point 2 X",
                Point2X,
                newPoint2X => Point2X = newPoint2X,
                marginLevel));

            var point2YField = (GetPropertyUI(
                "Point 2 Y",
                Point2Y,
                newPoint2Y => Point2Y = newPoint2Y,
                marginLevel));
            point2YField.Margin = standardMargin;
            infoPanel.Children.Add(point2YField);


            infoPanel.Children.Add(GetPropertyUI(
                "Point 3 X",
                Point3X,
                newPoint3X => Point3X = newPoint3X,
                marginLevel));

            var point3YField = (GetPropertyUI(
                "Point 3 Y",
                Point3Y,
                newPoint3Y => Point3Y = newPoint3Y,
                marginLevel));
            point3YField.Margin = standardMargin;
            infoPanel.Children.Add(point3YField);

        }
    }
}
