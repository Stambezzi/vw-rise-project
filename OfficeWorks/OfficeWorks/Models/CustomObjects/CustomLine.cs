﻿using OfficeWorks.ModelViews.Tools;
using OfficeWorks.ModelViews;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Linq;

namespace OfficeWorks.Models.CustomObjects
{
    internal class CustomLine : CustomObjectBase
    {
        private double _length;
        public double Length
        {
            get { return this._length; }
            set
            {
                this._length = Math.Max(value,0);
                if (GetUIElement() != null)
                {
                    this.Recreate();
                }
            }
        }

        public CustomLine(double positionX, double positionY, double rotation, double length, string name = "Line") : base(positionX, positionY, rotation, name)
        {
            this.Length = length;
        }

        public override IUIContainer CreateUI()
        {
            LineSegment line1 = CreateLineSegmentWithDistance(this.Length * ZoomHandler.ZoomLevelScale);

            PathFigure pathFigure = new PathFigure();
            pathFigure.StartPoint = new Point(0, 0);
            pathFigure.Segments.Add(line1);

            PathGeometry pathGeometry = new PathGeometry();
            pathGeometry.Figures.Add(pathFigure);

            Path line = new Path();
            line.Stroke = Brushes.Black;
            line.StrokeThickness = ScaleHandler.CalculateLineThickness();
            line.Data = pathGeometry;

            return new UIPathContainer(line);
        }

        public override void AddToInfoPanel(Panel infoPanel, int marginLevel)
        {
            base.AddToInfoPanel(infoPanel, marginLevel);
            Thickness standardMargin = new Thickness(5 * marginLevel, 0, 0, 10);

            var lengthField = (
                GetPropertyUI(
                    "Length",
                    Length,
                    newLength => Length = newLength,
                    marginLevel
                ));
            lengthField.Margin = standardMargin;
            infoPanel.Children.Add(lengthField);
        }
    }
}
