﻿using OfficeWorks.ModelViews.Tools;
using OfficeWorks.ModelViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using OfficeWorks.ModelViews.Tools.DrawTools;

namespace OfficeWorks.Models.CustomObjects
{
    internal class CustomCircle : CustomObjectBase
    {

        private double _radius;
        public double Radius
        {
            get { return this._radius; }
            set
            {
                this._radius = Math.Max(value, 0);
                if (GetUIElement() != null)
                {
                    Recreate();
                }
            }
        }

        public CustomCircle(double positionX, double positionY, double radius, double rotation, string name = "Circle") : base(positionX, positionY, rotation, name)
        {
            this.Radius = radius;
        }
        public override UIPathContainer CreateUI()
        {
            var radius = Radius * ZoomHandler.ZoomLevelScale;

            PathFigure pathFigure = new PathFigure();
            pathFigure.StartPoint = new Point(radius, 0);

            ArcSegment arcSegment1 = new ArcSegment();
            arcSegment1.Point = new Point(-radius, 0);
            arcSegment1.Size = new Size(radius, radius);
            arcSegment1.SweepDirection = SweepDirection.Clockwise;
            arcSegment1.IsLargeArc = true;

            ArcSegment arcSegment2 = new ArcSegment();
            arcSegment2.Point = new Point(radius, 0);
            arcSegment2.Size = new Size(radius, radius);
            arcSegment2.SweepDirection = SweepDirection.Clockwise;
            arcSegment2.IsLargeArc = true;

            pathFigure.Segments.Add(arcSegment1);
            pathFigure.Segments.Add(arcSegment2);

            pathFigure.IsClosed = true;

            PathGeometry pathGeometry = new PathGeometry();
            pathGeometry.Figures.Add(pathFigure);

            Path ellipse = new Path();
            ellipse.Stroke = Brushes.Black;
            ellipse.StrokeThickness = ScaleHandler.CalculateLineThickness(); ;
            ellipse.Data = pathGeometry;

            return new UIPathContainer(ellipse);
        }

        public override void AddToInfoPanel(Panel infoPanel, int marginLevel)
        {
            base.AddToInfoPanel(infoPanel, marginLevel);
            Thickness standardMargin = new Thickness(5 * marginLevel, 0, 0, 10);

            var radiusField = (
                GetPropertyUI(
                    "Radius",
                    Radius,
                    newRadius => Radius = newRadius,
                    marginLevel));
            radiusField.Margin = standardMargin;
            infoPanel.Children.Add(radiusField);

        }
    }
}
