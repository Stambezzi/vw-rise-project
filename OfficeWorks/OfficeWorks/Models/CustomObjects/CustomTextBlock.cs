﻿using OfficeWorks.ModelViews;
using OfficeWorks.ModelViews.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace OfficeWorks.Models.CustomObjects
{
    internal class CustomTextBlock : CustomObjectBase
    {
        public string? Text { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }

        public CustomTextBlock(double positionX, double positionY, double rotation, string text)
            : base(positionX, positionY, rotation, "Textblock") 
        {
            this.Text = text;
        }


        public override IUIContainer CreateUI()
        {
            var tb = new TextBlock();
            tb.FontSize = 16 * ZoomHandler.ZoomLevelScale;
            if (tb.FontSize > 16)
            {
                tb.FontSize = 16;
            }
            tb.Text = Text;
            tb.Width = Width;
            tb.Height = Height;
            tb.IsHitTestVisible = false;
            Transforms.Translate(tb, 0, ScaleHandler.CalculateLineThickness());
            return new UIElementContainer(tb);
        }
    }
}
