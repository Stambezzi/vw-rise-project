﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;

namespace OfficeWorks.Models.CustomObjects
{
    internal class CustomDimension : CustomComplexObj
    {
        public CustomDimension(double positionX, double positionY, List<CustomObjectBase> buildingObjects, double rotation) : base(positionX, positionY, buildingObjects, rotation)
        {
        }
    }
}
