﻿using OfficeWorks.ModelViews;
using OfficeWorks.ModelViews.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OfficeWorks.Models.CustomObjects
{
	internal class CustomChair : CustomComplexObj
	{
		double PillowRadius { get; set; }

		private int _legs;
		int Legs { get{ return _legs; } set
			{
				this._legs = Math.Min(Math.Max(value, 3), 6);
			}
		}

		double InterLegAngle { get { return 360.0 / Legs; } }

		bool HasBackStand { get; set; }
		public CustomChair(double positionX, double positionY, double rotation, double pillowRadius, int legs, bool hasBackStand=true ) : base(positionX, positionY, new List<CustomObjectBase>(), rotation,"Chair")
		{
			this.PillowRadius = pillowRadius;
			this.Legs = legs;
			this.HasBackStand = hasBackStand;

			this.BuildingObjects.Add(new CustomCircle(0, 0, pillowRadius, 0, "Seat"));

			if (this.HasBackStand)
			{
				var newPos = (new Point(PillowRadius, -PillowRadius));
				this.BuildingObjects.Add(new CustomRectangle(newPos.X, newPos.Y, 10, 2 * this.PillowRadius, 0, "Back Stand"));
				
			}

			List<CustomObjectBase> legsArr = new List<CustomObjectBase>();
			for (int i =1; i<=Legs; i++)
			{
				legsArr.Add(new CustomLine(0, 0, i * InterLegAngle, 1.1 * pillowRadius, $"Leg {i}"));
			}

			this.BuildingObjects.Add(new CustomComplexObj(0, 0, legsArr, 0, "Legs"));
		}
	}
}
