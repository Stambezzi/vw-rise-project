﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeWorks.Models.CustomObjects
{
    internal interface IBusinessObserver
    {
        public Guid UIID { get; set; }
    }
}
