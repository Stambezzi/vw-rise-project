﻿using OfficeWorks.ModelViews.Tools;
using OfficeWorks.ModelViews;
using System;
using System.Transactions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Media3D;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Xps;
using OfficeWorks.ModelViews.Tools.DrawTools;
using System.ComponentModel;

namespace OfficeWorks.Models.CustomObjects
{
    internal class CustomRectangle : CustomObjectBase
    {
        private double _width;
        public double Width
        {
            get { return this._width; }
            set
            {
                this._width = Math.Max(value, 0);
                if (GetUIElement() != null)
                {
                    Recreate();
                }
            }
        }

        private double _height;
        public double Height
        {
            get { return this._height; }
            set
            {
                this._height = Math.Max(value, 0);
                if (GetUIElement() != null)
                {
                    Recreate();
                }
            }
        }

        public CustomRectangle(double positionX, double positionY, double width, double height, double rotation, string name = "Rectangle")
            : base(positionX, positionY, rotation, name)
        {
            this.Width = width;
            this.Height = height;
        }
        public override IUIContainer CreateUI()
        {
            var screenWidthForUI = Width * ZoomHandler.ZoomLevelScale;
            var screenHeightForUI = Height * ZoomHandler.ZoomLevelScale;

            Point point = new Point(0, 0);
            point.X += screenWidthForUI;
            LineSegment line1 = new LineSegment(point, true);

            point.Y += screenHeightForUI;
            LineSegment line2 = new LineSegment(point, true);

            point.X -= screenWidthForUI;
            LineSegment line3 = new LineSegment(point, true);

            point.Y -= screenHeightForUI;
            //LineSegment line4 = new LineSegment(point, true);

            PathFigure figure = new PathFigure();
            figure.StartPoint = point;
            figure.Segments.Add(line1);
            figure.Segments.Add(line2);
            figure.Segments.Add(line3);
            figure.IsClosed = true;
            //figure.Segments.Add(line4);

            PathGeometry pathGeometry = new PathGeometry();
            pathGeometry.Figures.Add(figure);
            Path rect = new Path();
            rect.Stroke = Brushes.Black;
            rect.StrokeThickness = ScaleHandler.CalculateLineThickness();

            rect.Data = pathGeometry;

            return new UIPathContainer(rect);
        }

        public override void AddToInfoPanel(Panel infoPanel, int marginLevel)
        {
            base.AddToInfoPanel(infoPanel, marginLevel);
            Thickness standardMargin = new Thickness(5 * marginLevel, 0, 0, 10);

            infoPanel.Children.Add(
                GetPropertyUI(
                    "Width",
                    Width,
                    newWidth => Width = newWidth,
                    marginLevel));

            var heightField = (
                GetPropertyUI(
                    "Height",
                    Height,
                    newHeight => Height = newHeight,
                    marginLevel));
            heightField.Margin = standardMargin;
            infoPanel.Children.Add(heightField);
        }
    }
}