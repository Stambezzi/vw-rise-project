﻿using Newtonsoft.Json.Linq;
using OfficeWorks.ModelViews;
using OfficeWorks.ModelViews.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace OfficeWorks.Models.CustomObjects
{
    public class CustomComplexObj : CustomObjectBase
    {
        public List<CustomObjectBase> BuildingObjects;
        public CustomComplexObj(double positionX, double positionY, List<CustomObjectBase> buildingObjects, double rotation, string name = "Composite Object") : base(positionX, positionY, rotation, name)
        {
            this.BuildingObjects = new List<CustomObjectBase>(buildingObjects);
        }
        public override IUIContainer CreateUI()
            => InternalDraw();

        private UIComplexContainer InternalDraw()
        {
            UIComplexContainer complexObj = new UIComplexContainer();
            this.UIID = complexObj.ID;
			SetRootID(this);
			foreach (CustomObjectBase buildingObject in BuildingObjects)
            {
                var creation = buildingObject.Create(MainWindow.Instance.MainCanvas);

                MainWindow.Instance.MainCanvas.Children.Remove(creation.UI);
                FileManager.businessObjects.Remove(buildingObject);

                InitializePosition(
                    creation.UI,
                    new Point(buildingObject.PositionX * ZoomHandler.ZoomLevelScale, buildingObject.PositionY * ZoomHandler.ZoomLevelScale), 
                    buildingObject.Rotation, 
                    new Point(buildingObject.ScaleX, buildingObject.ScaleY));

                complexObj.Children.Add(creation.UI);
				
			}
           
            return complexObj;
        }

        private void SetRootID(CustomObjectBase element)
        {
            if (element is CustomComplexObj complexElement)
            {
                foreach (var item in complexElement.BuildingObjects)
                {
                    SetRootID(item);
                }
            }
            else
            {
                element.RootID = this.UIID;
            }
            if (element != this)
            {
                if (this.RootID == Guid.Empty)
                {
                    element.RootID = this.UIID;
                }
                else
                {
                    element.RootID = this.RootID;
                }
            }            
        }

        public override void AddToInfoPanel(Panel infoPanel, int marginLevel)
        {
            base.AddToInfoPanel(infoPanel, marginLevel);

            foreach (CustomObjectBase subObject in BuildingObjects)
            {
                if (subObject is CustomComplexObj complexObj)
                {
                    complexObj.AddToInfoPanel(infoPanel, marginLevel + 1);
                }
                else
                {
                    subObject.AddToInfoPanel(infoPanel, marginLevel + 1);
                }
            }
        }
    }
}
