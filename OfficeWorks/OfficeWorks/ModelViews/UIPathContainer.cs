﻿using OfficeWorks.Models.CustomObjects;
using OfficeWorks.ModelViews.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Xml.Linq;
using Path = System.Windows.Shapes.Path;

namespace OfficeWorks.ModelViews
{
    public class UIPathContainer : FrameworkElement, IUIContainer
    {
        public Guid ID { get { return _id; } set { _id = value; } }
        public CustomComplexObj Root { get; set; }
        public Path Shape = new Path();
        public FrameworkElement UI => this;

        public bool IsSelectable { get; set; } = true;

        private Guid _id = Guid.NewGuid();

        public UIPathContainer(Path newPath)
        {
            Shape = newPath;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            drawingContext.DrawGeometry(Shape.Fill, new Pen(Shape.Stroke, Shape.StrokeThickness) { DashStyle = new DashStyle(Shape.StrokeDashArray, 0) }, Shape.Data);
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            return new Size(0, 0);
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            return finalSize;
        }

        public void DrawTemp()
        {
            IsSelectable = false;
            Shape.Stroke = Brushes.Red;
            Shape.StrokeThickness = ScaleHandler.CalculateLineThickness() / 2;
            DoubleCollection dashes = new DoubleCollection { 2, 2 }; 
            Shape.StrokeDashArray = dashes;
        }
    }
}
