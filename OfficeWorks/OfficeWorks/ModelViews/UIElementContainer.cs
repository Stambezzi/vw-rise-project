﻿using OfficeWorks.Models.CustomObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Shapes;
using Path = System.Windows.Shapes.Path;

namespace OfficeWorks.ModelViews
{
    public class UIElementContainer : Canvas, IUIContainer
    {
        public Guid ID { get { return _id; } set { _id = value; } }
        public CustomComplexObj Root { get; set; }
        public FrameworkElement UI => this;
        public bool IsSelectable { get; set; } = true;

        private Guid _id = Guid.NewGuid();
        private readonly FrameworkElement Element;

        public UIElementContainer(FrameworkElement element)
        {
            Element = element;
            Children.Add(Element);
        }

        public void DrawTemp()
        {
            IsSelectable = false;
        }
    }
}
