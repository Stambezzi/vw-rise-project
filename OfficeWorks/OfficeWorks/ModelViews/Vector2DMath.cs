﻿using OfficeWorks.ModelViews.Tools;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace OfficeWorks.ModelViews
{
    public class Vector2DMath
    {
        public static Point ScreenToWorldPoint(Point screenPosition)
        {
            // Get the pan offset and zoom scale
            Vector panOffset = PanTool.PanOffset;
            double zoomScale = ZoomHandler.ZoomLevelScale;

            // Adjust the screen position based on the pan offset and zoom scale
            Point adjustedPosition = new Point(
                (screenPosition.X - panOffset.X) / zoomScale,
                (screenPosition.Y - panOffset.Y) / zoomScale);

            return adjustedPosition;
        }
        public static Point WorldToScreenPoint(Point worldPosition)
        {
            // Get the pan offset and zoom scale
            Vector panOffset = PanTool.PanOffset;
            double zoomScale = ZoomHandler.ZoomLevelScale;

            // Apply the grid's position to the world position
            Point adjustedPosition = new Point(
                (worldPosition.X * zoomScale) + panOffset.X,
                (worldPosition.Y * zoomScale) + panOffset.Y);

            return adjustedPosition;
        }
        public static double WorldToScreenPointX(double X)
        {
            // Get the pan offset and zoom scale
            Vector panOffset = PanTool.PanOffset;
            double zoomScale = ZoomHandler.ZoomLevelScale;

            // Apply the grid's position to the world position
            double adjustedPosition = (X * zoomScale) + panOffset.X;

            return adjustedPosition;
        }
        public static double WorldToScreenPointY(double Y)
        {
            // Get the pan offset and zoom scale
            Vector panOffset = PanTool.PanOffset;
            double zoomScale = ZoomHandler.ZoomLevelScale;

            // Apply the grid's position to the world position
            double adjustedPosition = (Y * zoomScale) + panOffset.Y;

            return adjustedPosition;
        }

        public static double GetRotationAngleBasedOnTwoPoints(Point firstPoint, Point secondPoint)
        {
            double deltaX = secondPoint.X - firstPoint.X;
            double deltaY = secondPoint.Y - firstPoint.Y;

            double angleInRadians = Math.Atan2(deltaY, deltaX);
            double angleInDegrees = angleInRadians * (180 / Math.PI);

            return angleInDegrees;
        }
        public static double CalculateWidthBasedOnTwoPoints(Point firstPoint, Point secondPoint)
        {
            return Math.Abs(firstPoint.X - secondPoint.X);
        }
        public static double CalculateHeightBasedOnTwoPoints(Point firstPoint, Point secondPoint)
        {
            return Math.Abs(firstPoint.Y - secondPoint.Y);
        }
        public static Point CalculateTopLeftPointBasedOnTwoPoints(Point firstPoint, Point secondPoint)
        {
            double left = Math.Min(firstPoint.X, secondPoint.X);
            double top = Math.Min(firstPoint.Y, secondPoint.Y);

            return new Point(left, top);
        }
        public static Point CalculateBottomRightPointBasedOnTwoPoints(Point firstPoint, Point secondPoint)
        {
            double right = Math.Max(firstPoint.X, secondPoint.X);
            double bottom = Math.Max(firstPoint.Y, secondPoint.Y);

            if (secondPoint.X > firstPoint.X)
            {
                right = secondPoint.X;
            }

            if (secondPoint.Y > firstPoint.Y)
            {
                bottom = secondPoint.Y;
            }

            return new Point(right, bottom);
        }
        public static double CalculateDistanceBetweenTwoPoints(Point firstPoint, Point secondPoint)
        {
            var distance = Math.Sqrt(Math.Pow(secondPoint.X - firstPoint.X, 2) + Math.Pow(secondPoint.Y - firstPoint.Y, 2));

            return distance;
        }
        public static LineSegment RotateLineSegment(LineSegment lineSegment, double rotationAngle, Point rotationCenter)
        {
            Point startPoint = lineSegment.Point;
            Point endPoint = new Point(lineSegment.Point.X + lineSegment.Point.X, lineSegment.Point.Y + lineSegment.Point.Y);

            // Apply rotation to the start and end points around the rotation center
            Point rotatedStartPoint = RotatePoint(startPoint, rotationAngle, rotationCenter);
            Point rotatedEndPoint = RotatePoint(endPoint, rotationAngle, rotationCenter);

            // Calculate the rotated vector based on the rotated start and end points
            Point rotatedVector = new Point(rotatedEndPoint.X - rotatedStartPoint.X, rotatedEndPoint.Y - rotatedStartPoint.Y);

            // Create a new rotated LineSegment
            LineSegment rotatedLineSegment = new LineSegment(rotatedVector, lineSegment.IsStroked);

            return rotatedLineSegment;
        }
        public static Point RotatePoint(Point point, double rotationAngle, Point rotationCenter)
        {
            double angleInRadians = rotationAngle * (Math.PI / 180.0);
            double cosTheta = Math.Cos(angleInRadians);
            double sinTheta = Math.Sin(angleInRadians);

            double translatedX = point.X - rotationCenter.X;
            double translatedY = point.Y - rotationCenter.Y;

            double rotatedX = (translatedX * cosTheta) - (translatedY * sinTheta);
            double rotatedY = (translatedX * sinTheta) + (translatedY * cosTheta);

            double finalX = rotatedX + rotationCenter.X;
            double finalY = rotatedY + rotationCenter.Y;

            return new Point(finalX, finalY);
        }
    }
}

