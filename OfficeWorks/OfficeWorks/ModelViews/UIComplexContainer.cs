﻿using OfficeWorks.Models.CustomObjects;
using OfficeWorks.ModelViews.Tools;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace OfficeWorks.ModelViews
{
    public class UIComplexContainer : Canvas, IUIContainer
    {
        public Guid ID { get { return _id; } set { _id = value; } }
        public CustomComplexObj Root { get; set; }
        public bool IsSelectable { get; set; } = true;

        private Guid _id = Guid.NewGuid();

        public FrameworkElement UI => this;

        public void DrawTemp()
        {
            IsSelectable = false;
            foreach (IUIContainer item in this.Children)
            {
                item.DrawTemp();
            }
        }
    }
}
