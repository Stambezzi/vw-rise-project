﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace OfficeWorks.ModelViews
{
    public class BackgroundGridLines : FrameworkElement
    {
        private List<Line> _gridLines = new List<Line>();
        private const int MAX_LINES = 16000;

        public BackgroundGridLines()
        {
            for (int x = -MAX_LINES; x <= MAX_LINES; x += 100)
            {
                Line verticalLine = new Line
                {
                    X1 = x,
                    Y1 = -MAX_LINES,
                    X2 = x,
                    Y2 = MAX_LINES,
                    StrokeThickness = (x % 1000 == 0) ? 6 : 2,
                    Stroke = Brushes.Gray
                };

                _gridLines.Add(verticalLine);
            }

            for (int y = -MAX_LINES; y <= MAX_LINES; y += 100)
            {
                Line horizontalLine = new Line
                {
                    X1 = -MAX_LINES,
                    Y1 = y,
                    X2 = MAX_LINES,
                    Y2 = y,
                    StrokeThickness = (y % 1000 == 0) ? 6 : 2,
                    Stroke = Brushes.Gray
                };

                _gridLines.Add(horizontalLine);
            }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            foreach (Line line in _gridLines)
            {
                drawingContext.DrawLine(new Pen(line.Stroke, line.StrokeThickness),
                    new Point(line.X1, line.Y1), new Point(line.X2, line.Y2));
            }
        }

        public void SetGridVisibility(Visibility value)
        {
            foreach (Line line in _gridLines)
            {
                line.Visibility = value;
            }

            InvalidateVisual();
        }
    }
}
