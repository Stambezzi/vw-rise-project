﻿using OfficeWorks.Models.CustomObjects;
using System;
using System.Windows;

namespace OfficeWorks.ModelViews
{
    /// <summary>
    /// Inherit along with a Framework element if you want it to work
    /// </summary>
    public interface IUIContainer
    {
        public Guid ID { get; set; }
        public bool IsSelectable { get; set; }
        public CustomComplexObj Root { get; set; }
        public FrameworkElement UI { get; }
        void DrawTemp();
    }
}