﻿using OfficeWorks.Models.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;

namespace OfficeWorks.ModelViews
{
    internal class InfoDisplay
    {
        public static void DisplayInfo(FrameworkElement selectedShape)
        {
            //var selectedShape = (Shape)MainWindow.Instance.MainCanvas.Children[0];
            //TODO get selected shape from user input
            var infoMenu = MainWindow.Instance.InfoMenu;
            CustomObjectBase businessObject = Models.FileManager.GetCustomObjectFromID(((IUIContainer)selectedShape).ID);
            if (businessObject != null)
            {
                businessObject.FillInfoPanel(infoMenu);
            }
        }

        public static void ClearInfo()
        {
            var infoMenu = MainWindow.Instance.InfoMenu;
            infoMenu.Children.Clear();
        }

    }
}
