﻿using OfficeWorks.ModelViews.Tools.DrawTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace OfficeWorks.ModelViews
{
    internal class Transforms
    {
        public static void Translate(FrameworkElement shapeToTransform, double offsetX = 0, double offsetY = 0)
        {
            Matrix transformMatrix = shapeToTransform.RenderTransform.Value;

            transformMatrix.Translate(offsetX, offsetY);

            shapeToTransform.RenderTransform = new MatrixTransform(transformMatrix);
        }

        internal static void TranslateTo(FrameworkElement shapeToTransform, double positionX, double positionY)
        {
            Matrix transformMatrix = shapeToTransform.RenderTransform.Value;
            Point currentPosition = ObjectInfo.GetPositionFromMatrix(transformMatrix);

            double offsetX = positionX - currentPosition.X;
            double offsetY = positionY - currentPosition.Y;

            Translate(shapeToTransform, offsetX, offsetY);
        }

        public static void Rotate(FrameworkElement shapeToTransform, double rotationAngle)
        {
            Matrix transformMatrix = shapeToTransform.RenderTransform.Value;
            Point centerOfRotation = ObjectInfo.GetPositionFromMatrix(transformMatrix);

            transformMatrix.RotateAt(rotationAngle, centerOfRotation.X, centerOfRotation.Y);

            shapeToTransform.RenderTransform = new MatrixTransform(transformMatrix);
        }
        internal static void RotateTo(FrameworkElement selectedObj, double rotationAngle)
        {
            //Get the existing angle
            double currentAngle = 0.0;
            if (selectedObj.RenderTransform is MatrixTransform matrixTransform)
            {
                var matrix = matrixTransform.Matrix;
                currentAngle = Math.Atan2(matrix.M21, matrix.M11) * (180.0 / Math.PI);
            }

            var finalAngle = (currentAngle + rotationAngle) % 360;

            Rotate(selectedObj, finalAngle);
        }

        public static void Rotate(FrameworkElement shapeToTransform, Point rotationPoint, double rotationAngle)
        {
            Matrix transformMatrix = shapeToTransform.RenderTransform.Value;

            transformMatrix.RotateAt(rotationAngle, rotationPoint.X, rotationPoint.Y);

            shapeToTransform.RenderTransform = new MatrixTransform(transformMatrix);
        }
        internal static void RotateTo(FrameworkElement selectedObj, Point rotationPoint, double rotationAngle)
        {
            //Get the existing angle
            double currentAngle = 0.0;
            if (selectedObj.RenderTransform is MatrixTransform matrixTransform)
            {
                var matrix = matrixTransform.Matrix;
                currentAngle = Math.Atan2(matrix.M21, matrix.M11) * (180.0 / Math.PI);
            }

            var finalAngle = (currentAngle + rotationAngle) % 360;

            Rotate(selectedObj, rotationPoint, finalAngle);
        }

        public static void Scale(FrameworkElement shapeToTransform, double scaleX, double scaleY)
        {
            Matrix transformMatrix = shapeToTransform.RenderTransform.Value;
            Point origin = ObjectInfo.GetPositionFromMatrix(transformMatrix);

            transformMatrix.ScaleAt(scaleX, scaleY, origin.X, origin.Y);

            shapeToTransform.RenderTransform = new MatrixTransform(transformMatrix);
        }
        public static void ScaleAt(FrameworkElement shapeToTransform, double scaleX, double scaleY, Point position)
        {
            Matrix transformMatrix = shapeToTransform.RenderTransform.Value;

            transformMatrix.ScaleAt(scaleX, scaleY, position.X, position.Y);

            shapeToTransform.RenderTransform = new MatrixTransform(transformMatrix);
        }

        public static void ScaleTo(FrameworkElement shapeToTransform, double desiredScaleX, double desiredScaleY) 
        {
            Matrix transformMatrix = shapeToTransform.RenderTransform.Value;

            Point currentScale = ObjectInfo.GetScaleFromMatrix(transformMatrix);

            double scaleFactorX = desiredScaleX / currentScale.X;
            double scaleFactorY = desiredScaleY / currentScale.Y;

            Scale(shapeToTransform, scaleFactorX, scaleFactorY);
         }
    }
}
