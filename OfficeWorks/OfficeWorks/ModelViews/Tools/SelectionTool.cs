using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows;
using System.Diagnostics;
using OfficeWorks.ModelViews.Tools.DrawTools;
using System.Windows.Input;
using System.Linq;
using OfficeWorks.Models.CustomObjects;
using System.ComponentModel;

namespace OfficeWorks.ModelViews.Tools
{
    internal class SelectionTool : ITool
    {
        private Canvas mainCanvas = MainWindow.Instance.MainCanvas;
        private static HashSet<IUIContainer>? _selectedObjs = new HashSet<IUIContainer>();
        public static HashSet<IUIContainer>? SelectedObjs => _selectedObjs;

        public Cursor Cursor => Cursors.Hand;

        /// <summary>
        /// Main selection function
        /// </summary>
        /// <param name="hitObject">The object to be selected</param>
        /// <returns>Collection of selected FrameworkElements</returns>
        public static ICollection<IUIContainer>? Select(IUIContainer? hitObject)
        {
            //Get parent group of selected object
            hitObject = SelectGroupParent(hitObject);
            Canvas mainCanvas = MainWindow.Instance.MainCanvas;

            if ((hitObject == null 
                || hitObject == mainCanvas 
                || _selectedObjs.Contains(hitObject))
                || !hitObject.IsSelectable)
            {
                // If hitObject is null or the mainCanvas, clear selection and return
                SetHighlight(false);
                _selectedObjs.Clear();
                return null;
            }

            SetHighlight(false);
            _selectedObjs.Clear();
            _selectedObjs.Add(hitObject);
            SetHighlight(true);
            DisplayInfoForSelectedObj();

            return _selectedObjs;
        }

        //Get the parent group of a FrameworkElement object
        public static IUIContainer? SelectGroupParent(IUIContainer clickedElement)
        {
            if (clickedElement != null)
            {
                if (clickedElement.Root == null)
                {
                    return clickedElement;
                }
                return clickedElement.Root.GetUIElement();
            }
            return null;
        }

        // Recursive function to traverse the visual tree and modify stroke color
        private static void SetHighlight(FrameworkElement element, bool isHighlighted)
        {
            if (element == null)
                return;
            if (element is UIPathContainer shape)
            {
                shape.Shape.Stroke = isHighlighted ? Brushes.SkyBlue : Brushes.Black;
                element.InvalidateVisual();
            }

            else if (element is Panel panel)
                foreach (var childElement in panel.Children)
                {
                    if (childElement is FrameworkElement childUIElement)
                    {
                        SetHighlight(childUIElement, isHighlighted); // Recursively call the function for nested elements
                    }
                }
        }
        private static void SetHighlight(bool isHighlighted)
        {
            foreach (var item in _selectedObjs)
            {
                SetHighlight(item.UI, isHighlighted);
            }
        }

        public void Activate()
        {
            //Select clicked object
            Point position = Vector2DMath.WorldToScreenPoint(DrawShapeTool.GetMouseWorldCoordinates());
            VisualTreeHelper.HitTest(mainCanvas, null, result =>
            {
                IUIContainer hitObject = result.VisualHit as IUIContainer;
                Select(hitObject);

                return HitTestResultBehavior.Stop;
            },
            new PointHitTestParameters(position));
            DisplayInfoForSelectedObj();
        }

        public void Deactivate()
        {
            //empty here
        }

        private static void DisplayInfoForSelectedObj()
        {
            //Display Info if only one element is selected
            if (SelectedObjs != null && SelectedObjs.Count == 1)
            {
                InfoDisplay.DisplayInfo(SelectGroupParent(SelectedObjs.First()).UI);
            }
            else
            {
                InfoDisplay.ClearInfo();
            }
        }
    }
}
