﻿using OfficeWorks.ModelViews.Tools.DrawTools;
using System.Windows.Input;

namespace OfficeWorks.ModelViews.Tools
{
    internal class ToolHandler
    {
        private ITool? _selectedTool;

        public ITool? SelectedTool
        {
            get { return _selectedTool; }
            set
            {
                if (value != null)
                {
                    Mouse.OverrideCursor = value.Cursor;
                }
                else
                {
                    Mouse.OverrideCursor = null;
                }
                _selectedTool = value; 
            }
        }

        public void ActivateSelectedTool()
        {
            if (SelectedTool != null)
            {
                SelectedTool.Activate();
            }
        }
    }
}
