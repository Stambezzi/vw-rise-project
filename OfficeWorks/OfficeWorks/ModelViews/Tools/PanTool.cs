﻿//canvas is moving properly but objects are fixed in one place
using OfficeWorks.Models;
using OfficeWorks.Models.CustomObjects;
using OfficeWorks.ModelViews.Tools.DrawTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;


namespace OfficeWorks.ModelViews.Tools
{
    internal class PanTool : IDragTool
    {
        private static Point _startPoint;
        private MatrixTransform _matrix = new MatrixTransform();
        private bool _isDragging;
        public Cursor Cursor => Cursors.ScrollAll;
        public static Vector PanOffset;

        public void Activate()
        {
            MouseDown();
        }
        public void Deactivate()
        {
            MouseUp();
        }

        public void MouseDown()
        {
            if (Mouse.MiddleButton == MouseButtonState.Pressed)
            {
                _startPoint = Mouse.GetPosition(MainWindow.Instance.MainCanvas);
                _isDragging = true;
            }
        }

        public void MouseMove()
        {
            if (Mouse.MiddleButton == MouseButtonState.Pressed && _isDragging)
            {
                Point mousePosition = Mouse.GetPosition(MainWindow.Instance.MainCanvas);
                PanScreen(mousePosition, _startPoint);
            }
        }

        private static void PanScreen(Point newPosition, Point startingPoint)
        {
            Vector delta = Point.Subtract(newPosition, startingPoint);

            foreach (FrameworkElement child in MainWindow.Instance.MainCanvas.Children)
            {
                // Apply the translation to the child element
                Transforms.Translate(child, delta.X, delta.Y);
            }

            PanOffset = new Vector(MainWindow.Instance.GridLines.RenderTransform.Value.OffsetX,
                MainWindow.Instance.GridLines.RenderTransform.Value.OffsetY);

            _startPoint = newPosition;
        }

        public static void ResetPan()
        {
            foreach (FrameworkElement child in MainWindow.Instance.MainCanvas.Children)
            {
                if (child is IUIContainer iuiContainer)
                {
                    // Apply the translation to the child element
                    Transforms.TranslateTo(child,
                        FileManager.GetCustomObjectFromID(iuiContainer.ID).PositionX,
                        FileManager.GetCustomObjectFromID(iuiContainer.ID).PositionY);
                }
                else
                {
                    // Apply the translation to the child element
                    Transforms.TranslateTo(child,
                        child.RenderTransform.Value.OffsetX - PanOffset.X,
                        child.RenderTransform.Value.OffsetY - PanOffset.Y);
                }
            }
        }

        public void MouseUp()
        {
            _isDragging = false;
        }
    }
}

