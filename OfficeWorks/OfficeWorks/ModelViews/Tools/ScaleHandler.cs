﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace OfficeWorks.ModelViews.Tools
{
    internal class ScaleHandler
    {
        private const double DefaultStrokeThickness = 5.0;

        public static void ResizeLineThickness(Canvas canvas)
        {
            //// Update the stroke thickness of all lines on the canvas
            //foreach (var line in canvas.Children.OfType<Shape>())
            //{
            //    line.StrokeThickness = CalculateLineThickness(canvas);
            //}
            //foreach (var complexObj in canvas.Children.OfType<Canvas>())
            //{
            //    ResizeLineThickness(complexObj);
            //}
        }

        public static double CalculateLineThickness()
        {
            return DefaultStrokeThickness * ZoomHandler.ZoomLevelScale;
        }
    }
}
