﻿using OfficeWorks.Models;
using OfficeWorks.Models.CustomObjects;
using OfficeWorks.ModelViews.Tools.DrawTools;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml.Linq;

namespace OfficeWorks.ModelViews.Tools
{
    internal class DeletionTool : ITool
    {
        public Cursor Cursor => Cursors.Hand;
        private static Canvas mainCanvas = MainWindow.Instance.MainCanvas;

        public void Activate()
        {
            //Select clicked object
            Point position = Vector2DMath.WorldToScreenPoint(DrawShapeTool.GetMouseWorldCoordinates());
            VisualTreeHelper.HitTest(mainCanvas, null, result =>
            {
                UIPathContainer hitObject = result.VisualHit as UIPathContainer;
                Delete(hitObject);

                return HitTestResultBehavior.Stop;
            },
            new PointHitTestParameters(position));
        }



        public static void Delete(IUIContainer element)
        {
            if (element != null)
            {
                if (SelectionTool.SelectedObjs.Contains(SelectionTool.SelectGroupParent(element)))
                {
                    SelectionTool.SelectedObjs.Remove(SelectionTool.SelectGroupParent(element));
                    InfoDisplay.ClearInfo();
                }
                if (element is IUIContainer uiElement)
                {
                    if (uiElement.Root != null)
                    {
                        FileManager.businessObjects.Remove(uiElement.Root);
                    }
                    else
                    {
                        FileManager.businessObjects.Remove(FileManager.GetCustomObjectFromID(uiElement.ID));
                    }
                }
                if (SelectionTool.SelectGroupParent(element) != null && mainCanvas.Children.Contains(SelectionTool.SelectGroupParent(element).UI))
                {
                    mainCanvas.Children.Remove(SelectionTool.SelectGroupParent(element).UI);
                }
            }
        }

        public void Deactivate()
        {
            //empty here
        }
    }
}
