﻿using OfficeWorks.Models;
using OfficeWorks.Models.CustomObjects;
using OfficeWorks.ModelViews.Tools.DrawTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shapes;

namespace OfficeWorks.ModelViews.Tools
{
    internal class SnapHandler
    {
        public static bool IsSnapToggled
        {
            get
            {
                if (MainWindow.Instance.ToggleSnapping.IsChecked == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                MainWindow.Instance.ToggleSnapping.IsChecked = value;
            } 
        }
        public static bool IsSnapped = false;
        public static double SnapStrength = 25;
        private static Path _snapPoint = new CustomCircle(0, 0, 5, 0).CreateUI().Shape;

        public static void HideSnapPoint()
        {
            _snapPoint.Visibility = Visibility.Hidden;
        }

        public static void DisplaySnapPoint(object sender, MouseEventArgs e)
        {
            if (!IsSnapToggled)
            {
                return;
            }
            if (!MainWindow.Instance.MainCanvas.Children.Contains(_snapPoint))
            {
                MainWindow.Instance.MainCanvas.Children.Add(_snapPoint);
                _snapPoint.StrokeThickness = 2.5;
                _snapPoint.IsHitTestVisible = false;
            }
            var pos = Vector2DMath.WorldToScreenPoint(Snap(DrawShapeTool.GetMouseWorldCoordinates()));
            Transforms.TranslateTo(_snapPoint, pos.X, pos.Y);
            if (IsSnapped)
            {
                _snapPoint.Visibility = Visibility.Visible;
            }
            else
            {
                _snapPoint.Visibility = Visibility.Hidden;
            }
        }

        public static Point Snap(Point position)
        {
            if (!IsSnapToggled)
            {
                IsSnapped = false;
                return position;
            }
            var snapX = Math.Round(position.X / 100) * 100;
            var snapY = Math.Round(position.Y / 100) * 100;

            var distanceX = position.X - snapX;
            var distanceY = position.Y - snapY;
            var distance = Math.Sqrt(distanceX * distanceX + distanceY * distanceY);

            if (distance <= SnapStrength)
            {
                position = new Point(snapX, snapY);
                IsSnapped = true;
            }
            else
            {
                IsSnapped = false;
            }

            return position;
        }
    }
}
