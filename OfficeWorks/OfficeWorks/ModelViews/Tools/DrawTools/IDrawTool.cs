﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeWorks.ModelViews.Tools.DrawTools
{
    internal interface IDrawTool : ITool
    {
        abstract void MouseMove();

    }
}
