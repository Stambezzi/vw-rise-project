﻿using System.Windows.Controls;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using OfficeWorks.Models.CustomObjects;
using System;

namespace OfficeWorks.ModelViews.Tools.DrawTools
{
    internal class DrawShapeTool
    {
        private static Canvas mainCanvas = MainWindow.Instance.MainCanvas;

        public static Point GetMouseWorldCoordinates()
        {
            // Get mouse coordinates
            Point mousePosition = Vector2DMath.ScreenToWorldPoint(Mouse.GetPosition(mainCanvas));

            // TODO: Insert Snapping logic here
            mousePosition = SnapHandler.Snap(mousePosition);

            return mousePosition;
        }
    }
}
