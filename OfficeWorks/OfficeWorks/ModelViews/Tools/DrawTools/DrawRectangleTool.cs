﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
using System.Windows.Shapes;
using System.Windows.Input;
using OfficeWorks.ModelViews;
using Microsoft.VisualBasic;
using System.Windows.Ink;
using OfficeWorks.Models.CustomObjects;

namespace OfficeWorks.ModelViews.Tools.DrawTools
{
	internal class DrawRectangleTool : DrawShapeTool, IDrawTool
	{
		public Cursor Cursor => Cursors.Pen;
		private int _clicked = 0;
		private Point firstPoint;
		private Point secondPoint;
		private Point thirdPoint;
		private Path rectangle = null;

		IUIContainer tempRectangle;
		Point startingPoint;
		double width;
		double height;

		public static IUIContainer DrawRectangle(Point startPoint, double width, double height)
		{
			CustomRectangle rect = new CustomRectangle(startPoint.X, startPoint.Y, width, height, 0);

            return rect.Create(MainWindow.Instance.MainCanvas);
        }

		public static IUIContainer DrawTempRectangle(Point startPoint, double width, double height)
		{
			CustomRectangle rect = new CustomRectangle(startPoint.X, startPoint.Y, width, height, 0);

            return rect.CreateTemp(MainWindow.Instance.MainCanvas);
        }

		public void Activate()
		{
			if (_clicked == 0)
			{
				firstPoint = GetMouseWorldCoordinates();
				_clicked++;
			}
			else if (_clicked == 1)
			{

				UtilityClass.DeletePreviewObj(tempRectangle);

				secondPoint = GetMouseWorldCoordinates();

				startingPoint = Vector2DMath.CalculateTopLeftPointBasedOnTwoPoints(firstPoint, secondPoint);
				width = Vector2DMath.CalculateWidthBasedOnTwoPoints(firstPoint, secondPoint);
				height = Vector2DMath.CalculateHeightBasedOnTwoPoints(firstPoint, secondPoint);

                DrawRectangle(startingPoint, width, height);
                _clicked++;
                _clicked = 0;
            }
        }
        public void MouseMove()
        {
            if (_clicked == 1)
            {
                UtilityClass.DeletePreviewObj(tempRectangle);

				secondPoint = GetMouseWorldCoordinates();

				startingPoint = Vector2DMath.CalculateTopLeftPointBasedOnTwoPoints(firstPoint, secondPoint);
				width = Vector2DMath.CalculateWidthBasedOnTwoPoints(firstPoint, secondPoint);
				height = Vector2DMath.CalculateHeightBasedOnTwoPoints(firstPoint, secondPoint);

                tempRectangle = DrawTempRectangle(startingPoint, width, height);
            }
        }

        public void Deactivate()
        {
            UtilityClass.DeletePreviewObj(tempRectangle);
        }
    }
}
