﻿using System.Windows.Input;

namespace OfficeWorks.ModelViews.Tools.DrawTools
{
    internal interface ITool
    {
        public void Activate();
        void Deactivate();

        public Cursor Cursor { get; }

    }
}
