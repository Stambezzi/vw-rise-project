﻿using OfficeWorks.Models.CustomObjects;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using static System.Net.Mime.MediaTypeNames;
using Point = System.Windows.Point;

namespace OfficeWorks.ModelViews.Tools.DrawTools
{
    internal class DrawDimensionTool : DrawShapeTool, IDrawTool
    {
        private bool _isFirstClick = true;
        private Point startPosition;
        IUIContainer _currentObj;

        private CustomDimension GenerateCustomDimention(Point mousePosition)
        {
            double length = Vector2DMath.CalculateDistanceBetweenTwoPoints(startPosition, mousePosition);
            var rotation = Vector2DMath.GetRotationAngleBasedOnTwoPoints(startPosition, mousePosition);
            List<CustomObjectBase> dimensionParts = new List<CustomObjectBase>()
                {
                    new CustomLine(0, 0, rotation, length),
                    new CustomTextBlock(0, 0, rotation, $"The distance is {(int)length} cm"){
                        Height = 40,
                        Width = 300
                    }
                };

            var customDimension = new CustomDimension(startPosition.X, startPosition.Y, dimensionParts, 0);
            return customDimension;
        }
        public Cursor Cursor => Cursors.Cross;

        private IUIContainer DrawDimensionTool_(Point mousePosition)
        {
            var customDimension = GenerateCustomDimention(mousePosition);
            return customDimension.Create(MainWindow.Instance.MainCanvas);
        }

        private IUIContainer DrawTempDimensionTool_(Point mousePosition)
        {
            var customDimension = GenerateCustomDimention(mousePosition);
            return customDimension.CreateTemp(MainWindow.Instance.MainCanvas);
        }

        public void Activate()
        {
            var mousePosition = GetMouseWorldCoordinates();

            if (_isFirstClick)
            {
                startPosition = mousePosition;
            }
            else
            {
                UtilityClass.DeletePreviewObj(_currentObj);
                DrawDimensionTool_(mousePosition);
            }
            _isFirstClick = !_isFirstClick;
        }

        public void Deactivate()
        {
            UtilityClass.DeletePreviewObj(_currentObj);
        }

        public void MouseMove()
        {
            if (!_isFirstClick)
            {
                UtilityClass.DeletePreviewObj(_currentObj);
                var mousePosition = GetMouseWorldCoordinates();
                _currentObj = DrawTempDimensionTool_(mousePosition);
            }
        }
    }
}
