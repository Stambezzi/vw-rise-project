﻿using OfficeWorks.Models.CustomObjects;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Point = System.Windows.Point;

namespace OfficeWorks.ModelViews.Tools.DrawTools
{
	internal class DrawLineTool : DrawShapeTool, IDrawTool
	{
		private bool _isFirstClick = true;
		private Point startPosition;
		IUIContainer currentLine;

		public Cursor Cursor => Cursors.Pen;

		private static CustomLine generateLine(Point startPoint, Point endPoint)
		{
			var length = Vector2DMath.CalculateDistanceBetweenTwoPoints(startPoint, endPoint);
			var rotation = Vector2DMath.GetRotationAngleBasedOnTwoPoints(startPoint, endPoint);

            var line = new CustomLine(startPoint.X, startPoint.Y, rotation, length);
            return line;
        }
        public static IUIContainer DrawLine(Point startPoint, Point endPoint)
        {
            var line = generateLine(startPoint, endPoint);
            return line.Create(MainWindow.Instance.MainCanvas);
        }
        public static IUIContainer DrawTempLine(Point startPoint, Point endPoint)
        {
            var line = generateLine(startPoint, endPoint);
            return line.CreateTemp(MainWindow.Instance.MainCanvas);
        }


		public void Activate()
		{
			var mousePosition = GetMouseWorldCoordinates();

			if (_isFirstClick)
			{
				startPosition = mousePosition;
			}
			else
			{
				UtilityClass.DeletePreviewObj(currentLine);

                DrawLine(startPosition, mousePosition);
            }
            _isFirstClick = !_isFirstClick;
        }
        public void MouseMove()
        {
            if (_isFirstClick == false)
            {
                UtilityClass.DeletePreviewObj(currentLine);
                var mousePosition = GetMouseWorldCoordinates();

				currentLine = DrawTempLine(startPosition, mousePosition);
				currentLine.UI.IsHitTestVisible = false;
			}
		}

		public void Deactivate()
		{
			UtilityClass.DeletePreviewObj(currentLine);
		}
	}
}
