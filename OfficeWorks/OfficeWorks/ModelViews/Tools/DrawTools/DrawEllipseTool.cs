﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Drawing;
using Point = System.Windows.Point;
using Size = System.Windows.Size;
using OfficeWorks.Models.CustomObjects;

namespace OfficeWorks.ModelViews.Tools.DrawTools
{
	internal class DrawEllipseTool : DrawShapeTool, IDrawTool
	{
		public Cursor Cursor => Cursors.Pen;
		private int _clicked;
		private Point _firstPoint;
		private Point _secondPoint;

		IUIContainer _currentObj;
		Point startingPoint;
		Point endPoint;
		double centerX;
		double centerY;
		double radius;

		public static IUIContainer DrawEllipse(Point startPoint, double radiusX, double radiusY)
		{
			var ellipse = new CustomCircle(startPoint.X, startPoint.Y, radiusX, 0);

            return ellipse.Create(MainWindow.Instance.MainCanvas);
        }
        public static IUIContainer DrawTempEllipse(Point startPoint, double radiusX, double radiusY)
        {
            var ellipse = new CustomCircle(startPoint.X, startPoint.Y, radiusX, 0);
            return ellipse.CreateTemp(MainWindow.Instance.MainCanvas);
        }
        public static IUIContainer DrawCircle(Point startPoint, double radius)
        {
            return DrawEllipse(startPoint, radius, radius);
        }
        public static IUIContainer DrawTempCircle(Point startPoint, double radius)
        {
            return DrawTempEllipse(startPoint, radius, radius);
        }


		public void Activate()
		{
			if (_clicked == 0)
			{
				_firstPoint = GetMouseWorldCoordinates();
				_clicked++;
			}
			else if (_clicked == 1)
			{
				UtilityClass.DeletePreviewObj(_currentObj);
				CustomObjectBase.RenderShape(DrawEllipse(new Point(centerX, centerY), radius, radius));
				_clicked = 0;
			}
		}
		public void MouseMove()
		{
			if (_clicked == 1)
			{

				UtilityClass.DeletePreviewObj(_currentObj);

				_secondPoint = GetMouseWorldCoordinates();
				startingPoint = Vector2DMath.CalculateTopLeftPointBasedOnTwoPoints(_firstPoint, _secondPoint);
				endPoint = Vector2DMath.CalculateBottomRightPointBasedOnTwoPoints(_firstPoint, _secondPoint);

				centerX = _firstPoint.X;
				centerY = _firstPoint.Y;

				radius = Vector2DMath.CalculateDistanceBetweenTwoPoints(endPoint, startingPoint);
                
                _currentObj = DrawTempEllipse(new Point(centerX, centerY), radius, radius);
            }
        }

		public void Deactivate()
		{
			UtilityClass.DeletePreviewObj(_currentObj);
		}
	}
}
