﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OfficeWorks.ModelViews.Tools.DrawTools
{
    internal interface IDragTool : ITool
    {
        abstract void MouseUp();
        abstract void MouseDown();
        abstract void MouseMove();
    }
}
