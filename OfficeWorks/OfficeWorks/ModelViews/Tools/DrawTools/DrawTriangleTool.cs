﻿using System.Windows.Input;
using System.Windows.Media;
using System.Windows;
using System.Windows.Shapes;
using OfficeWorks.Models.CustomObjects;

namespace OfficeWorks.ModelViews.Tools.DrawTools
{
    internal class DrawTriangleTool : DrawShapeTool, IDrawTool
    {
        public Cursor Cursor => Cursors.Pen;
        private int _clicked = 0;
        private Point firstPoint;
        private Point secondPoint;
        private Point thirdPoint;

        private IUIContainer _line;
        private IUIContainer _tempTriangle;

        public static IUIContainer DrawTriangle(Point firstPoint, Point secondPoint, Point thirdPoint)
        {
            var triangle = new CustomTriangle(firstPoint.X, firstPoint.Y, secondPoint.X, secondPoint.Y, thirdPoint.X, thirdPoint.Y, 0);

            return triangle.Create(MainWindow.Instance.MainCanvas);
        }
        public static IUIContainer DrawTempTriangle(Point firstPoint, Point secondPoint, Point thirdPoint)
        {
            var triangle = new CustomTriangle(firstPoint.X, firstPoint.Y, secondPoint.X, secondPoint.Y, thirdPoint.X, thirdPoint.Y, 0);

            return triangle.CreateTemp(MainWindow.Instance.MainCanvas);
        }

        public void Activate()
        {
            if (_clicked == 0)
            {
                firstPoint = GetMouseWorldCoordinates();
                _clicked++;
            }
            else if (_clicked == 1)
            {
                secondPoint = GetMouseWorldCoordinates();
                _clicked++;
            }
            else if (_clicked == 2)
            {
                UtilityClass.DeletePreviewObj(_line);
                UtilityClass.DeletePreviewObj(_tempTriangle);

                thirdPoint = GetMouseWorldCoordinates();
                CustomObjectBase.RenderShape(DrawTriangle(firstPoint, secondPoint, thirdPoint));

                _clicked = 0;
            }
        }

        public void Deactivate()
        {
            UtilityClass.DeletePreviewObj(_line);
            UtilityClass.DeletePreviewObj(_tempTriangle);
        }

        public void MouseMove()
        {
            if (_clicked == 1)
            {
                UtilityClass.DeletePreviewObj(_line);
                var mousePosition = GetMouseWorldCoordinates();
                _line = DrawLineTool.DrawTempLine(firstPoint, mousePosition);
            }
            else if (_clicked == 2)
            {
                UtilityClass.DeletePreviewObj(_tempTriangle);
                thirdPoint = GetMouseWorldCoordinates();
                _tempTriangle = DrawTempTriangle(firstPoint, secondPoint, thirdPoint);
            }
        }
    }
}
