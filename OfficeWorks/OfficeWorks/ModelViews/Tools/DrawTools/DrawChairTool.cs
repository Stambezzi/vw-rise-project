﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shapes;
using OfficeWorks.Models.CustomObjects;

namespace OfficeWorks.ModelViews.Tools.DrawTools
{
    internal class DrawChairTool : DrawShapeTool, IDrawTool
    {
        IUIContainer _currentObj;
        public Cursor Cursor => Cursors.Cross;

        private static CustomChair GenerateChair(Point startPoint, double radius)
        {
            bool hasBackStand = true;
            int legs = 4;

            return new CustomChair(startPoint.X, startPoint.Y, 0, radius, legs, hasBackStand);
        }
        public static IUIContainer DrawChair(Point startPoint, double radius)
        {
           var chair = GenerateChair(startPoint,radius);
            return chair.Create(MainWindow.Instance.MainCanvas);
        }
        public static IUIContainer DrawTempChair(Point startPoint, double radius)
        {
            var chair = GenerateChair(startPoint, radius);
            return chair.CreateTemp(MainWindow.Instance.MainCanvas);
        }

		public void Activate()
		{
			UtilityClass.DeletePreviewObj(_currentObj);
			var mousePosition = GetMouseWorldCoordinates();
			DrawChair(mousePosition, 50);
		}

        public void Deactivate()
        {
            UtilityClass.DeletePreviewObj(_currentObj);
        }

        public void MouseMove()
        {
            UtilityClass.DeletePreviewObj(_currentObj);
            var mousePosition = GetMouseWorldCoordinates();
            _currentObj = DrawTempChair(mousePosition, 50);
        }
    }
}
