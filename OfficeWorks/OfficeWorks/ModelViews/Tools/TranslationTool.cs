﻿using OfficeWorks.Models;
using OfficeWorks.ModelViews.Tools.DrawTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace OfficeWorks.ModelViews.Tools
{
    internal class TranslationTool : ITool
    {
        public Cursor Cursor => Cursors.Hand;

        public void Activate()
        {
            if (SelectionTool.SelectedObjs.Count != 1)
            {
                return;
            }

            Point position = Vector2DMath.WorldToScreenPoint(DrawShapeTool.GetMouseWorldCoordinates());
            var selectedObj = SelectionTool.SelectedObjs.First().UI;

            if (selectedObj is IUIContainer uiElement)
            {
                var businessObj = FileManager.GetCustomObjectFromID(uiElement.ID);
                businessObj.TranslateBy(Vector2DMath.ScreenToWorldPoint(position).X - businessObj.PositionX,
                    Vector2DMath.ScreenToWorldPoint(position).Y - businessObj.PositionY);
            }

            //Transforms.TranslateTo(selectedObj,
            //    position.X,
            //    position.Y);
            InfoDisplay.DisplayInfo(selectedObj);
        }

        public void Deactivate()
        {
            //empty here
        }
    }
}
