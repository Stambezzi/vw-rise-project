﻿using OfficeWorks.Models;
using OfficeWorks.ModelViews.Tools.DrawTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace OfficeWorks.ModelViews.Tools
{
    internal class RotateTool : ITool
    {
        public Cursor Cursor => Cursors.Cross;

        public void Activate()
        {
            if (SelectionTool.SelectedObjs.Count > 0)
            {
                Point position = DrawShapeTool.GetMouseWorldCoordinates();
                var selectedObj = SelectionTool.SelectedObjs.First().UI;
                var objectPosition = Vector2DMath.ScreenToWorldPoint(
                    new Point(
                        selectedObj.RenderTransform.Value.OffsetX, 
                        selectedObj.RenderTransform.Value.OffsetY));

                // Calculate the rotation angle
                var rotationAngle = Vector2DMath.GetRotationAngleBasedOnTwoPoints(
                    new Point(objectPosition.X, objectPosition.Y),
                    position);

                if (selectedObj is IUIContainer uiElement)
                {
                    var businessObj = FileManager.GetCustomObjectFromID(uiElement.ID);
                    businessObj.RotateTo(rotationAngle);
                }

                //Transforms.RotateTo(selectedObj, rotationAngle);
                InfoDisplay.DisplayInfo(selectedObj);
            }
        }

        public void Deactivate()
        {
           //empty here
        }
    }
}
