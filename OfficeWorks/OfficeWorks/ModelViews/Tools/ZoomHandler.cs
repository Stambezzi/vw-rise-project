﻿using OfficeWorks.Models;
using OfficeWorks.Models.CustomObjects;
using OfficeWorks.ModelViews.Tools.DrawTools;
using OfficeWorks.View.UserControls;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace OfficeWorks.ModelViews.Tools
{
    internal class ZoomHandler : ITool
    {
        public Cursor Cursor => Cursors.ScrollNS;
        private static Canvas mainCanvas = MainWindow.Instance.MainCanvas;
        public static double ZoomLevelScale = 1;
        public static double MinZoom = 1;
        public static double MaxZoom = 10000;

        public void Activate()
        {
            //We are holding down shift
            if (Keyboard.Modifiers == ModifierKeys.Shift)
            {
                Zoom(0.5);
            }
            //We aren't holding down shift
            else
            {
                Zoom(2.0);
            }
        }

        public void Deactivate()
        {
            //empty here
        }

        public static void ResetCanvasPosition()
        {
            ZoomBar.ZoomTextBox.Text = "100";
            //PanTool.ResetPan();
        }

        public static void Zoom(double zoomValue)
        {
            var newScaleValue = ZoomLevelScale * zoomValue;
            SetZoom(newScaleValue);
        }

        public static void SetZoom(double inputZoomPercentage)
        {
            if (inputZoomPercentage == 0)
            {
                return;
            }

            var scaleModifier = inputZoomPercentage / ZoomLevelScale / 100;

            //TODO add global and custom working with CustomShapeObjects

            foreach (FrameworkElement child in mainCanvas.Children)
            {
                Transforms.ScaleAt(child, scaleModifier, scaleModifier,
                    new Point(
                        MainWindow.Instance.CanvasGrid.ActualWidth / 2,
                        MainWindow.Instance.CanvasGrid.ActualHeight / 2));
            }

            ZoomLevelScale *= scaleModifier;
            PanTool.PanOffset = new Vector(MainWindow.Instance.GridLines.RenderTransform.Value.OffsetX,
                MainWindow.Instance.GridLines.RenderTransform.Value.OffsetY);
            ScaleHandler.ResizeLineThickness(mainCanvas);
        }
    }
}
