﻿using OfficeWorks.Models;
using OfficeWorks.ModelViews.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace OfficeWorks.ModelViews
{

    static public class UtilityClass
    {
        static public void DeletePreviewObj(IUIContainer element)
        {
            if (element != null)
            {
                if(SelectionTool.SelectedObjs.Contains(SelectionTool.SelectGroupParent(element)))
                {
                    SelectionTool.SelectedObjs.Remove(SelectionTool.SelectGroupParent(element));
                    InfoDisplay.ClearInfo();
                }
                if (MainWindow.Instance.MainCanvas.Children.Contains(SelectionTool.SelectGroupParent(element).UI))
                {
                    MainWindow.Instance.MainCanvas.Children.Remove(SelectionTool.SelectGroupParent(element).UI);
                }
            }
        }
    }
}
