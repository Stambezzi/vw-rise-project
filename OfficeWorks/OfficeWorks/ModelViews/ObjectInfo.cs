﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace OfficeWorks.ModelViews
{
	internal class ObjectInfo
	{
		public static double GetRotationFromMatrix(Matrix transformationMatrix)
		{
			double rotation = Math.Atan2(transformationMatrix.M12, transformationMatrix.M11);
			rotation *= 180 / Math.PI;

			return rotation;
		}

		public static Point GetScaleFromMatrix(Matrix transformationMatrix)
		{
			Matrix helperMatrix = transformationMatrix;
			double scaleX = Math.Sqrt(helperMatrix.M11 * helperMatrix.M11 + helperMatrix.M12 * helperMatrix.M12);
			double scaleY = Math.Sqrt(helperMatrix.M21 * helperMatrix.M21 + helperMatrix.M22 * helperMatrix.M22);
			
			return new Point(scaleX,scaleY);
		}
		
		public static Point GetPositionFromMatrix(Matrix transformationMatrix)
		{
			double posX = transformationMatrix.OffsetX;
			double posY = transformationMatrix.OffsetY;

			return new Point(posX, posY);
		}
	}
}
