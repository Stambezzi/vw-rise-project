﻿using OfficeWorks.ModelViews.Tools;
using System;
using System.Windows.Controls;
using System.Windows.Input;

namespace OfficeWorks.View.UserControls
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ZoomBar : UserControl
    {
        public static TextBox ZoomTextBox;

        public ZoomBar()
        {
            InitializeComponent();
            ZoomTextBox = ZoomTextField;
            ZoomTextBox.TextChanged += ActivateZoom;
        }

        private void ActivateZoom(object sender, TextChangedEventArgs e)
        {
            if (double.TryParse(ZoomTextBox.Text, out double currentZoom))
            {
                currentZoom = Math.Clamp(currentZoom, ZoomHandler.MinZoom, ZoomHandler.MaxZoom);
                if (currentZoom != 0)
                {
                    ZoomHandler.SetZoom(currentZoom);
                    ZoomTextBox.TextChanged -= ActivateZoom;
                    ZoomTextBox.Text = currentZoom.ToString("F2");
                    //ZoomTextBox.CaretIndex = ZoomTextBox.Text.Length;
                    ZoomTextBox.TextChanged += ActivateZoom;
                }
            }
            else
            {
                return;
            }
        }
        public static void HandleMouseWheelZoom(object sender, MouseWheelEventArgs e)
        {
            if (double.TryParse(ZoomTextBox.Text, out double currentZoom))
            {
                double delta = e.Delta / Mouse.MouseWheelDeltaForOneLine;
                currentZoom = Math.Clamp(currentZoom, ZoomHandler.MinZoom, ZoomHandler.MaxZoom);
                double zoomValue = currentZoom;
                double zoomAmount = 1.1;

                if (delta > 0)
                {
                    zoomValue *= zoomAmount;
                }
                else if (delta < 0)
                {
                    zoomValue /= zoomAmount;
                }

                ZoomTextBox.Text = zoomValue.ToString();
                ZoomTextBox.CaretIndex = ZoomTextBox.Text.Length;
            }
        }
    }
}
