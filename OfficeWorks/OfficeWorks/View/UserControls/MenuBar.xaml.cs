﻿
using OfficeWorks.ModelViews;
using OfficeWorks.Models.CustomObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OfficeWorks.Models;
using OfficeWorks.ModelViews.Tools.DrawTools;
using System.Windows.Media.Animation;
using OfficeWorks.ModelViews.Tools;
using System.Runtime.Intrinsics.X86;
using System.Transactions;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Media3D;

namespace OfficeWorks.View.UserControls
{
    public partial class MenuBar : UserControl
    {

        public MenuBar()
        {
            InitializeComponent();
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            var vm = new FileManager();
            vm.OpenFile();
        }

        private void Import_Click(object sender, RoutedEventArgs e)
        {
            var vm = new FileManager();
            vm.ImportFile();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            FileManager fm = new FileManager();
            fm.SaveFile();
        }

        private void SaveAs_Click(object sender, RoutedEventArgs e)
        {
            var vm = new FileManager();
            vm.SaveFileAs();
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            var vm = new FileManager();
            vm.ClearCanvas();
        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            string infoText = "Welcome to the Help Center!\n\nIn this panel, you'll find a variety of tools that can assist you in creating and manipulating objects. Let's take a closer look at each tool and its functionalities:\n\n🗑️ Deletion Tool (1 click):\nWith a single click, you can remove unwanted objects from your canvas.\n\n📏 Line Tool (2 clicks):\nWith just two clicks, you can create straight lines of any length or orientation. The line tool offers a preview, allowing you to adjust its position before finalizing.\n\n🔳 Rectangle Tool (2 clicks):\nCreating rectangles is a breeze with this tool. Simply click twice to define the opposite corners of the rectangle. Like other tools, the rectangle tool provides a preview so you can modify its size and proportions as needed.\n\n📐 Triangle Tool (3 clicks):\nConstructing triangles is made easy with the triangle tool. Click three times to define the vertices, and the tool will automatically connect them to form a triangle. The preview feature ensures that you can adjust the shape before confirming.\n\n⚪ Circle Tool (2 clicks):\nNeed perfect circles? Look no further. The circle tool allows you to create circles by clicking twice to define the diameter. The preview helps you tweak the size and position before confirming.\n\n🪑 Chair Tool (1 click):\nWith just a single click, you can add chairs to your design. The chair tool conveniently places a pre-designed chair object in your workspace.\n\nFor the next two tools - Translation ↑ and Rotation ↻, you need to select the wanted object and then modify its state.\nSimplified - Right-click on the wanted object, then when it's colored in blue - choose the Translation or Rotation tool.\n\n⚟ Dimension Tool (2 clicks):\nAccurately measure distances between objects using the dimension tool. Click on two points, and it will display the length or width between them. This tool is handy for ensuring precise placements and alignments.\n\nTo navigate the workspace, use the pan tool by pressing the middle mouse button. This allows you to move around and explore your design effortlessly.\n\nTo select a tool, simply click the left mouse button on its respective icon in the Tool Panel.\n\nTo create an object, click the left mouse button after choosing a tool. The object will appear in black, indicating that it has been successfully created.\n\nTo select existing objects, use the right mouse button. Clicking on an object will highlight it, allowing you to manipulate or modify its properties.\n\nIn preview mode, objects are displayed in red, helping you distinguish them from the finalized objects in black.\n\nWith this comprehensive set of tools and intuitive controls, you'll have everything you need to bring your creative visions to life in the Canvas. Enjoy OfficeWorks!";
            MessageBox.Show(infoText, "Help Center", MessageBoxButton.OK, MessageBoxImage.Question);
        }

        private void View_Click(object sender, RoutedEventArgs e)
        {
            ZoomHandler.ResetCanvasPosition();
        }

        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            string editInstructions = "Welcome to Edit!\n\nFirst - select the object you wish to edit by clicking on it.\n\nRight - click on the selected object to open the context menu.\n\nFrom the context menu, choose the appropriate editing tool based on your needs:\n\n↑Translation Tool: Use this tool to move the object to a different location.\n🗑️Deletion Tool: Select this tool to remove the object from the scene entirely.\n↻Rotation Tool: Use this tool to rotate the object around its axis.";
            MessageBox.Show(editInstructions, "Edit", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}

//Welcome to Edit!\n
//\nFirst - select the object you wish to edit by clicking on it.\n
//\tRight - click on the selected object to open the context menu.
//\nFrom the context menu, choose the appropriate editing tool based on your needs:\n
//\n\t↑Translation Tool: Use this tool to move the object to a different location.
//\n\t🗑️Deletion Tool: Select this tool to remove the object from the scene entirely.
//\n\t↻Rotation Tool: Use this tool to rotate the object around its axis.


//Welcome to the Help Center!\n
//In this panel, you'll find a variety of tools that can assist you in creating and manipulating objects. Let's take a closer look at each tool and its functionalities:\n
//\n🗑️Deletion Tool(1 click):\n
//With a single click you can remove unwanted objects from your canvas.\n
//\n📏Line Tool(2 clicks):\n
//With just two clicks, you can create straight lines of any length or orientation. The line tool offers a preview, allowing you to adjust its position before finalizing.\n
//\n🔳Rectangle Tool (2 clicks):\n
//Creating rectangles is a breeze with this tool. Simply click twice to define the opposite corners of the rectangle. Like other tools, the rectangle tool provides a preview so you can modify its size and proportions as needed.\n
//\n📐Triangle Tool (3 clicks):\n
//Constructing triangles is made easy with the triangle tool. Click three times to define the vertices, and the tool will automatically connect them to form a triangle. The preview feature ensures that you can adjust the shape before confirming.\n
//\n⚪Circle Tool (2 clicks):\n
//Need perfect circles? Look no further. The circle tool allows you to create circles by clicking twice to define the diameter. The preview helps you tweak the size and position before confirming.\n
//\n🪑Chair Tool (1 click):\n
//With just a single click, you can add chairs to your design. The chair tool conveniently places a pre-designed chair object in your workspace.\n
//\nFor the next two tools - Translation ↑ and Rotation ↻, you need to select the wanted object and then modify it's state.\n
//Simplified - Right click on the wanted object, then when it's coloured in blue - choose Translation or Rotation tool.\n
//\n⚟Dimension Tool (2 clicks):\n
//\nAccurately measure distances between objects using the dimension tool. Click on two points, and it will display the length or width between them. This tool is handy for ensuring precise placements and alignments.\n
//\nTo navigate the workspace, use the pan tool by pressing the middle mouse button. This allows you to move around and explore your design effortlessly.\n
//\nTo select a tool, simply click the left mouse button on its respective icon in the Tool Panel.\n
//\nTo create an object, click the left mouse button after choosing a tool. The object will appear in black, indicating that it has been successfully created.
//\nTo select existing objects, use the right mouse button. Clicking on an object will highlight it, allowing you to manipulate or modify its properties.\n
//\nIn preview mode, objects are displayed in red, helping you distinguish them from the finalized objects in black.\n
//\nWith this comprehensive set of tools and intuitive controls, you'll have everything you need to bring your creative visions to life in the Canvas. Enjoy OfficeWorks!\n