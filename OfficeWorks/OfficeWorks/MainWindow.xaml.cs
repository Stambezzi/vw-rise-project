﻿using OfficeWorks.Models;
using OfficeWorks.Models.CustomObjects;
using OfficeWorks.ModelViews;
using OfficeWorks.ModelViews.Tools;
using OfficeWorks.ModelViews.Tools.DrawTools;
using OfficeWorks.View.UserControls;
using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OfficeWorks
{
	public partial class MainWindow : Window
	{
		private ITool oldTool = null;
		private ToolHandler toolHandler = new ToolHandler();

		public static MainWindow Instance { get; private set; }
		public BackgroundGridLines GridLines;

        public MainWindow()
        {
            InitializeComponent();
            Instance = this;
            InitializeBackground();            
        }

        public void InitializeBackground()
        {
            GridLines = new BackgroundGridLines();
            ////add the gridlines when objects are stored in "custom object"
            MainCanvas.Children.Add(GridLines);
            Line line1 = new Line();
            line1.X1 = 0;
            line1.Y1 = 0;
            line1.X2 = 100;
            line1.Y2 = 0;
            line1.Stroke = Brushes.Blue;
            line1.StrokeThickness = ScaleHandler.CalculateLineThickness();
            MainCanvas.Children.Add(line1);
            Line line2 = new Line();
            line2.X1 = 0;
            line2.Y1 = 0;
            line2.X2 = 0;
            line2.Y2 = 100;
            line2.Stroke = Brushes.Red;
            line2.StrokeThickness = ScaleHandler.CalculateLineThickness();
            MainCanvas.Children.Add(line2);
        }

		private void ClickActivateTool(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			toolHandler.ActivateSelectedTool();
		}

		private void OnMouseDownActivateTool(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (toolHandler.SelectedTool is IDragTool)
			{
				((IDragTool)toolHandler.SelectedTool).MouseDown();
			}
		}
		private void OnMouseMoveActivateTool(object sender, System.Windows.Input.MouseEventArgs e)
		{
			SnapHandler.DisplaySnapPoint(sender, e);
			if (toolHandler.SelectedTool is IDragTool)
			{
				((IDragTool)toolHandler.SelectedTool).MouseMove();
			}

			if (toolHandler.SelectedTool is IDrawTool)
			{
				((IDrawTool)toolHandler.SelectedTool).MouseMove();
			}

		}
		private void OnMouseUPActivateTool(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			if (toolHandler.SelectedTool is IDragTool)
			{
				((IDragTool)toolHandler.SelectedTool).MouseUp();
			}
		}

        private void SwitchTool(ITool? tool)
        {
            toolHandler.SelectedTool?.Deactivate();
            toolHandler.SelectedTool = tool;
        }
        private void SelectLineTool(object sender, RoutedEventArgs e)
        {
            SwitchTool(new DrawLineTool());
        }
        private void SelectRectangleTool(object sender, RoutedEventArgs e)
        {
            SwitchTool(new DrawRectangleTool());
        }
        private void SelectCircleTool(object sender, RoutedEventArgs e)
        {
            SwitchTool(new DrawEllipseTool());
        }
        private void SelectChairTool(object sender, RoutedEventArgs e)
        {
            SwitchTool(new DrawChairTool());
        }
        private void SelectSelectionTool(object sender, RoutedEventArgs e)
        {
            SwitchTool(new SelectionTool());
        }
        private void SelectDeletionTool(object sender, RoutedEventArgs e)
        {
            SwitchTool(new DeletionTool());
        }
        private void SelectTriangleTool(object sender, RoutedEventArgs e)
        {
            SwitchTool(new DrawTriangleTool());
        }
        private void SelectRotateTool(object sender, RoutedEventArgs e)
        {
            SwitchTool(new RotateTool());
        }
        private void SelectTranslationTool(object sender, RoutedEventArgs e)
        {
            SwitchTool(new TranslationTool());
        }
        private void SelectCalculateDistanceTool(object sender, RoutedEventArgs e)
        {
            SwitchTool(new DrawDimensionTool());
        }
        private void MainWindow_OnKeyPressed(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                SwitchTool(null);
            }
            if (e.Key == Key.Space)
            {
                ZoomHandler.ResetCanvasPosition();
            }
        }

		private void HandleMouseWheelZoom(object sender, MouseWheelEventArgs e)
		{
			ZoomBar.HandleMouseWheelZoom(sender, e);
		}

		private void OnWindowMouseLeave(object sender, MouseEventArgs e)
		{
			if (toolHandler.SelectedTool is IDragTool)
			{
				((IDragTool)toolHandler.SelectedTool).MouseUp();
			}
		}

		public IUIContainer? GetContainer(Guid id)
		{
			return GetContainer(id, MainCanvas);
		}

		public IUIContainer? GetContainer(FrameworkElement element)
		{
			if (element is IUIContainer container)
			{
				return container;
			}
			return null;
		}
		public IUIContainer? GetContainer(Guid id, Canvas canvas)
		{
			foreach (FrameworkElement child in canvas.Children)
			{
				IUIContainer container = GetContainer(child);
				if (container!=null && container.ID == id)
				{
					return container;
				}
				else if (child is UIComplexContainer subCanvas)
				{
					IUIContainer? res = GetContainer(id, subCanvas);
					if (res != null)
					{
						return res;
					}
				}
			}

			return null;
		}

        private void SelectPanTool()
        {
            toolHandler.SelectedTool = new PanTool();
        }

		private void TrySelectPanTool(object sender, MouseButtonEventArgs e)
		{
			if (e.ButtonState == Mouse.MiddleButton)
			{
				oldTool = toolHandler.SelectedTool;
				SelectPanTool();
				toolHandler.ActivateSelectedTool();
			}
		}

        private void TrySelectObject(object sender, MouseButtonEventArgs e)
        {
            oldTool = toolHandler.SelectedTool;
            toolHandler.SelectedTool = new SelectionTool();

            toolHandler.ActivateSelectedTool();
            toolHandler.SelectedTool = oldTool;
        }

        private void TryDeselectPanTool(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Middle)
            {
                toolHandler.SelectedTool = oldTool;
                oldTool = null;
            }
        }

        private void OnMainCanvasMouseLeave(object sender, MouseEventArgs e)
        {
            SnapHandler.HideSnapPoint();
            if (toolHandler.SelectedTool is IDragTool)
            {
                toolHandler.SelectedTool = oldTool;
                oldTool = null;
            }
        }
    }
}
